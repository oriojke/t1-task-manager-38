package ru.t1.didyk.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.didyk.taskmanager.api.service.IConnectionService;
import ru.t1.didyk.taskmanager.marker.UnitCategory;
import ru.t1.didyk.taskmanager.model.Session;

import java.util.Arrays;

import static ru.t1.didyk.taskmanager.constant.ProjectTestData.SESSION;
import static ru.t1.didyk.taskmanager.constant.ProjectTestData.USER1;

@Category(UnitCategory.class)
public final class SessionServiceTest {

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(new PropertyService());

    @NotNull
    private final SessionService sessionService = new SessionService(connectionService);

    @Before
    public void before() {
        sessionService.add(SESSION);
    }

    @After
    public void after() {
        sessionService.clear(USER1.getId());
    }

    @Test
    public void add() {
        Assert.assertNotNull(sessionService.add(new Session()));
        Assert.assertNotNull(sessionService.add(USER1.getId(), new Session()));
        Assert.assertFalse(sessionService.add(Arrays.asList(new Session(), new Session())).isEmpty());
    }

    @Test
    public void clear() {
        sessionService.clear(USER1.getId());
        Assert.assertTrue(sessionService.findAll(USER1.getId()).isEmpty());
    }

    @Test
    public void find() {
        Assert.assertFalse(sessionService.findAll().isEmpty());
        Assert.assertFalse(sessionService.findAll(USER1.getId()).isEmpty());
        Assert.assertNotNull(sessionService.findOneById(USER1.getId(), SESSION.getId()));
        Assert.assertNotNull(sessionService.findOneByIndex(USER1.getId(), 0));
    }

    @Test
    public void remove() {
        Assert.assertNotNull(sessionService.remove(SESSION));
    }

    @Test
    public void removeAll() {
        sessionService.removeAll(USER1.getId());
        Assert.assertTrue(sessionService.findAll(USER1.getId()).isEmpty());
    }

    @Test
    public void removeById() {
        Assert.assertNotNull(sessionService.removeById(USER1.getId(), SESSION.getId()));
    }

    @Test
    public void removeByIndex() {
        Assert.assertNotNull(sessionService.removeByIndex(USER1.getId(), 0));
    }

}
