package ru.t1.didyk.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.didyk.taskmanager.api.service.IConnectionService;
import ru.t1.didyk.taskmanager.marker.UnitCategory;
import ru.t1.didyk.taskmanager.model.User;
import ru.t1.didyk.taskmanager.service.ConnectionService;
import ru.t1.didyk.taskmanager.service.PropertyService;

import java.util.Arrays;

import static ru.t1.didyk.taskmanager.constant.ProjectTestData.USER1;
import static ru.t1.didyk.taskmanager.constant.ProjectTestData.USER2;

@Category(UnitCategory.class)
public final class UserRepositoryTest {

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(new PropertyService());

    @NotNull
    private final UserRepository userRepository = new UserRepository(connectionService.getConnection());

    @Before
    public void before() {
        userRepository.add(USER1);
        userRepository.add(USER2);
    }

    @After
    public void after() {
        userRepository.clear();
    }

    @Test
    public void add() {
        Assert.assertNotNull(userRepository.add(new User()));
        Assert.assertFalse(userRepository.add(Arrays.asList(new User(), new User())).isEmpty());
    }

    @Test
    public void clear() {
        userRepository.clear();
        Assert.assertTrue(userRepository.findAll().isEmpty());
    }

    @Test
    public void find() {
        Assert.assertFalse(userRepository.findAll().isEmpty());
        Assert.assertNotNull(userRepository.findById(USER1.getId()));
        Assert.assertNotNull(userRepository.findByLogin(USER1.getLogin()));
        Assert.assertNotNull(userRepository.findByEmail(USER1.getEmail()));
        Assert.assertNotNull(userRepository.findOneByIndex(0));
        Assert.assertNotNull(userRepository.findOneById(USER1.getId()));
    }

    @Test
    public void exists() {
        Assert.assertTrue(userRepository.isEmailExists(USER1.getEmail()));
        Assert.assertTrue(userRepository.isLoginExists(USER1.getLogin()));
    }

    @Test
    public void remove() {
        Assert.assertNotNull(userRepository.remove(USER1));
    }

    @Test
    public void removeAll() {
        userRepository.removeAll(Arrays.asList(USER1, USER2));
        Assert.assertTrue(userRepository.findAll().isEmpty());
    }

    @Test
    public void removeById() {
        Assert.assertNotNull(userRepository.removeById(USER1.getId()));
    }

    @Test
    public void removeByIndex() {
        Assert.assertNotNull(userRepository.removeByIndex(0));
    }

}
