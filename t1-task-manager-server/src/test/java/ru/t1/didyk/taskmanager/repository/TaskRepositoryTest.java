package ru.t1.didyk.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.didyk.taskmanager.api.service.IConnectionService;
import ru.t1.didyk.taskmanager.enumerated.Sort;
import ru.t1.didyk.taskmanager.marker.UnitCategory;
import ru.t1.didyk.taskmanager.model.Task;
import ru.t1.didyk.taskmanager.service.ConnectionService;
import ru.t1.didyk.taskmanager.service.PropertyService;

import java.util.ArrayList;
import java.util.Arrays;

import static ru.t1.didyk.taskmanager.constant.ProjectTestData.*;

@Category(UnitCategory.class)
public final class TaskRepositoryTest {

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(new PropertyService());

    @NotNull
    private final TaskRepository taskRepository = new TaskRepository(connectionService.getConnection());

    @Before
    public void before() {
        taskRepository.add(USER_TASK);
        taskRepository.add(ADMIN_TASK);
    }

    @After
    public void after() {
        taskRepository.clear();
    }

    @Test
    public void add() {
        Assert.assertNotNull(taskRepository.add(USER_TASK2));
        Assert.assertNotNull(taskRepository.add(USER2.getId(), USER_TASK3));
        Assert.assertNotNull(taskRepository.add(USER_TASK_LIST));
        Assert.assertNull(taskRepository.add(NULL_STRING, USER_TASK3));
    }

    @Test
    public void create() {
        Assert.assertNotNull(taskRepository.create(USER1.getId(), "TEST NAME"));
        Assert.assertNotNull(taskRepository.create(USER1.getId(), "TEST NAME 2", "TEST DESCRIPTION"));
    }

    @Test
    public void clear() {
        taskRepository.clear(USER2.getId());
        Assert.assertTrue(taskRepository.findAll(USER2.getId()).isEmpty());
        taskRepository.clear();
        Assert.assertTrue(taskRepository.findAll().isEmpty());
    }

    @Test
    public void find() {
        Assert.assertNotNull(taskRepository.findOneById(USER_TASK.getId()));
        Assert.assertNotNull(taskRepository.findOneById(USER1.getId(), USER_TASK.getId()));
        Assert.assertNull(taskRepository.findOneById(NULL_STRING, USER_TASK.getId()));
        Assert.assertNull(taskRepository.findOneById(USER1.getId(), NULL_STRING));
        Assert.assertNull(taskRepository.findOneById(NULL_STRING, NULL_STRING));
        Assert.assertNotNull(taskRepository.findOneByIndex(0));
        Assert.assertNotNull(taskRepository.findOneByIndex(USER1.getId(), 0));
        Assert.assertFalse(taskRepository.findAll().isEmpty());
        Assert.assertFalse(taskRepository.findAll(USER1.getId()).isEmpty());
        Assert.assertTrue(taskRepository.findAll(NULL_STRING).isEmpty());
        Assert.assertFalse(taskRepository.findAll(USER1.getId(), Sort.BY_NAME.getComparator()).isEmpty());
        Assert.assertFalse(taskRepository.findAll(USER1.getId(), Sort.BY_CREATED.getComparator()).isEmpty());
        Assert.assertFalse(taskRepository.findAll(USER1.getId(), Sort.BY_STATUS.getComparator()).isEmpty());
        Assert.assertFalse(taskRepository.findAll(Sort.BY_NAME.getComparator()).isEmpty());
        Assert.assertFalse(taskRepository.findAll(Sort.BY_CREATED.getComparator()).isEmpty());
        Assert.assertFalse(taskRepository.findAll(Sort.BY_CREATED.getComparator()).isEmpty());
        Assert.assertNotNull(taskRepository.findAllByProjectId(USER1.getId(), USER_PROJECT.getId()));
    }

    @Test
    public void getSize() {
        Assert.assertTrue(taskRepository.getSize() > 0);
        Assert.assertTrue(taskRepository.getSize(USER1.getId()) > 0);
    }

    @Test
    public void remove() {
        Assert.assertNotNull(taskRepository.remove(ADMIN_TASK));
        Assert.assertNotNull(taskRepository.remove(USER1.getId(), USER_TASK));
        Assert.assertNull(taskRepository.remove(NULL_STRING, NULL_TASK));
    }

    @Test
    public void removeById() {
        Assert.assertNotNull(taskRepository.removeById(USER_TASK.getId()));
        Assert.assertNull(taskRepository.removeById(NULL_STRING, NULL_STRING));
        Assert.assertNull(taskRepository.removeById(USER2.getId(), "RaNdOmStRiNg"));
        Assert.assertNull(taskRepository.removeById(NULL_STRING, ADMIN_TASK.getId()));
        Assert.assertNull(taskRepository.removeById(USER2.getId(), NULL_STRING));
        Assert.assertNotNull(taskRepository.removeById(USER2.getId(), ADMIN_TASK.getId()));
    }

    @Test
    public void removeByIndex() {
        Assert.assertNotNull(taskRepository.removeByIndex(0));
        Assert.assertThrows(RuntimeException.class, () -> taskRepository.removeByIndex(USER2.getId(), 15));
        Assert.assertNotNull(taskRepository.removeByIndex(USER2.getId(), 0));
    }

    @Test
    public void removeAll() {
        Assert.assertThrows(RuntimeException.class, () -> taskRepository.removeAll(NULL_STRING));
        taskRepository.removeAll(USER1.getId());
        taskRepository.removeAll(new ArrayList<Task>(Arrays.asList(USER_TASK, ADMIN_TASK)));
        Assert.assertTrue(taskRepository.findAll().isEmpty());
    }

}
