package ru.t1.didyk.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.didyk.taskmanager.api.service.IConnectionService;
import ru.t1.didyk.taskmanager.enumerated.Sort;
import ru.t1.didyk.taskmanager.marker.UnitCategory;
import ru.t1.didyk.taskmanager.model.Project;
import ru.t1.didyk.taskmanager.service.ConnectionService;
import ru.t1.didyk.taskmanager.service.PropertyService;

import java.util.ArrayList;
import java.util.Arrays;

import static ru.t1.didyk.taskmanager.constant.ProjectTestData.*;

@Category(UnitCategory.class)
public final class ProjectRepositoryTest {

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(new PropertyService());

    @NotNull
    private final ProjectRepository projectRepository = new ProjectRepository(connectionService.getConnection());

    @Before
    public void before() {
        projectRepository.add(USER_PROJECT);
        projectRepository.add(ADMIN_PROJECT);
    }

    @After
    public void after() {
        projectRepository.clear();
    }

    @Test
    public void add() {
        Assert.assertNotNull(projectRepository.add(USER_PROJECT2));
        Assert.assertNotNull(projectRepository.add(USER2.getId(), USER_PROJECT3));
        Assert.assertNotNull(projectRepository.add(USER_PROJECT_LIST));
        Assert.assertNull(projectRepository.add(NULL_STRING, USER_PROJECT3));
    }

    @Test
    public void create() {
        Assert.assertNotNull(projectRepository.create(USER1.getId(), "TEST NAME"));
        Assert.assertNotNull(projectRepository.create(USER1.getId(), "TEST NAME 2", "TEST DESCRIPTION"));
    }

    @Test
    public void clear() {
        projectRepository.clear(USER2.getId());
        Assert.assertTrue(projectRepository.findAll(USER2.getId()).isEmpty());
        projectRepository.clear();
        Assert.assertTrue(projectRepository.findAll().isEmpty());
    }

    @Test
    public void find() {
        Assert.assertNotNull(projectRepository.findOneById(USER_PROJECT.getId()));
        Assert.assertNotNull(projectRepository.findOneById(USER1.getId(), USER_PROJECT.getId()));
        Assert.assertNull(projectRepository.findOneById(NULL_STRING, NULL_STRING));
        Assert.assertNotNull(projectRepository.findOneByIndex(0));
        Assert.assertNotNull(projectRepository.findOneByIndex(USER1.getId(), 0));
        Assert.assertFalse(projectRepository.findAll().isEmpty());
        Assert.assertFalse(projectRepository.findAll(USER1.getId()).isEmpty());
        Assert.assertTrue(projectRepository.findAll(NULL_STRING).isEmpty());
        Assert.assertFalse(projectRepository.findAll(USER1.getId(), Sort.BY_NAME.getComparator()).isEmpty());
        Assert.assertFalse(projectRepository.findAll(USER1.getId(), Sort.BY_CREATED.getComparator()).isEmpty());
        Assert.assertFalse(projectRepository.findAll(USER1.getId(), Sort.BY_STATUS.getComparator()).isEmpty());
        Assert.assertFalse(projectRepository.findAll(Sort.BY_NAME.getComparator()).isEmpty());
        Assert.assertFalse(projectRepository.findAll(Sort.BY_CREATED.getComparator()).isEmpty());
        Assert.assertFalse(projectRepository.findAll(Sort.BY_CREATED.getComparator()).isEmpty());
    }

    @Test
    public void getSize() {
        Assert.assertTrue(projectRepository.getSize() > 0);
        Assert.assertTrue(projectRepository.getSize(USER1.getId()) > 0);
    }

    @Test
    public void remove() {
        Assert.assertNotNull(projectRepository.remove(ADMIN_PROJECT));
        Assert.assertNotNull(projectRepository.remove(USER1.getId(), USER_PROJECT));
        Assert.assertNull(projectRepository.remove(NULL_STRING, NULL_PROJECT));
    }

    @Test
    public void removeById() {
        Assert.assertNotNull(projectRepository.removeById(USER_PROJECT.getId()));
        Assert.assertNull(projectRepository.removeById(NULL_STRING, NULL_STRING));
        Assert.assertNull(projectRepository.removeById(USER2.getId(), "RaNdOmStRiNg"));
        Assert.assertNotNull(projectRepository.removeById(USER2.getId(), ADMIN_PROJECT.getId()));
    }

    @Test
    public void removeByIndex() {
        Assert.assertNotNull(projectRepository.removeByIndex(0));
        Assert.assertThrows(RuntimeException.class, () -> projectRepository.removeByIndex(USER2.getId(), 15));
        Assert.assertNotNull(projectRepository.removeByIndex(USER2.getId(), 0));
    }

    @Test
    public void removeAll() {
        Assert.assertThrows(RuntimeException.class, () -> projectRepository.removeAll(NULL_STRING));
        projectRepository.removeAll(USER1.getId());
        projectRepository.removeAll(new ArrayList<Project>(Arrays.asList(USER_PROJECT, ADMIN_PROJECT)));
        Assert.assertTrue(projectRepository.findAll().isEmpty());
    }

}
