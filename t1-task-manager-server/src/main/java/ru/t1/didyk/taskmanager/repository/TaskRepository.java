package ru.t1.didyk.taskmanager.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.api.repository.ITaskRepository;
import ru.t1.didyk.taskmanager.enumerated.Status;
import ru.t1.didyk.taskmanager.model.Task;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    public TaskRepository(@NotNull Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    public Task create(@NotNull final String userId, @NotNull final String name, @NotNull final String description) {
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        return add(task);
    }

    @NotNull
    @Override
    public Task create(@NotNull final String userId, @NotNull final String name) {
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setUserId(userId);
        return add(task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAllByProjectId(@NotNull final String userId, @Nullable final String projectId) {
        @NotNull final List<Task> result = new ArrayList<>();
        @NotNull final String sql = String.format("select * from %s where user_id='%s' and project_id='%s'", getTableName(), userId, projectId);
        try {
            @NotNull final Statement statement = connection.createStatement();
            @NotNull final ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next())
                result.add(fetch(resultSet));
        } catch (Exception e) {
            throw e;
        }
        return result;
    }

    @Override
    protected String getTableName() {
        return "tasks";
    }

    @Override
    @SneakyThrows
    protected @NotNull Task fetch(@NotNull ResultSet row) {
        @NotNull Task task = new Task();
        task.setId(row.getString("id"));
        task.setName(row.getString("name"));
        task.setDescription(row.getString("description"));
        task.setUserId(row.getString("user_id"));
        task.setStatus(Status.toStatus(row.getString("status")));
        task.setCreated(row.getTimestamp("created"));
        task.setProjectId(row.getString("project_id"));
        return task;
    }

    @Override
    @SneakyThrows
    public @Nullable Task add(@Nullable Task model) {
        @NotNull final String sql = String.format("insert into %s (id, name, description, user_id, created, status, project_id)" +
                "values (?, ?, ?, ?, ?, ?, ?)", getTableName());
        try {
            @NotNull PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, model.getId());
            statement.setString(2, model.getName());
            statement.setString(3, model.getDescription());
            statement.setString(4, model.getUserId());
            statement.setTimestamp(5, new Timestamp(model.getCreated().getTime()));
            statement.setString(6, Status.NOT_STARTED.toString());
            statement.setString(7, model.getProjectId());
            statement.executeUpdate();
        } catch (Exception e) {
            throw e;
        }
        return model;
    }

    @Override
    @SneakyThrows
    public void update(@NotNull Task object) {
        @NotNull final String sql = String.format("update %s set name=?, description=?, user_id=?, created=?, status=?, project_id=?" +
                "where id=?", getTableName());
        try {
            @NotNull PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, object.getName());
            statement.setString(2, object.getDescription());
            statement.setString(3, object.getUserId());
            statement.setTimestamp(4, new Timestamp(object.getCreated().getTime()));
            statement.setString(5, object.getStatus().toString());
            statement.setString(6, object.getProjectId());
            statement.setString(7, object.getId());
            statement.executeUpdate();
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public @Nullable Task add(@Nullable String userId, @NotNull Task model) {
        if (userId == null) return null;
        model.setUserId(userId);
        return add(model);
    }
}
