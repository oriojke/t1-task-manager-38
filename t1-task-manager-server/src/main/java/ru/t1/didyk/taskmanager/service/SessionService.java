package ru.t1.didyk.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.didyk.taskmanager.api.repository.ISessionRepository;
import ru.t1.didyk.taskmanager.api.service.IConnectionService;
import ru.t1.didyk.taskmanager.api.service.ISessionService;
import ru.t1.didyk.taskmanager.model.Session;
import ru.t1.didyk.taskmanager.repository.SessionRepository;

import java.sql.Connection;

public class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    public SessionService(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    public @NotNull void update(@NotNull Session object) {

    }

    @Override
    protected @NotNull ISessionRepository getRepository(@NotNull Connection connection) {
        return new SessionRepository(connection);
    }
}
