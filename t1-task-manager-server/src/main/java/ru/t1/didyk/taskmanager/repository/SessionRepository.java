package ru.t1.didyk.taskmanager.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.api.repository.ISessionRepository;
import ru.t1.didyk.taskmanager.enumerated.Role;
import ru.t1.didyk.taskmanager.model.Session;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;

public class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

    public SessionRepository(@NotNull Connection connection) {
        super(connection);
    }

    @Override
    protected String getTableName() {
        return "sessions";
    }

    @Override
    @SneakyThrows
    protected @NotNull Session fetch(@NotNull ResultSet row) {
        @NotNull Session session = new Session();
        session.setUserId(row.getString("user_id"));
        session.setDate(row.getTimestamp("created"));
        session.setRole(Role.valueOf(row.getString("role")));
        session.setId(row.getString("id"));
        return session;
    }

    @Override
    @SneakyThrows
    public @Nullable Session add(@Nullable Session model) {
        @NotNull final String sql = String.format("insert into %s (id , user_id, created, role)" +
                "values (?, ?, ?, ?)", getTableName());
        try {
            @NotNull PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, model.getId());
            statement.setString(2, model.getUserId());
            statement.setTimestamp(3, new Timestamp(model.getDate().getTime()));
            statement.setString(4, model.getRole().toString());
            statement.executeUpdate();
        } catch (Exception e) {
            throw e;
        }
        return model;
    }

    @Override
    @SneakyThrows
    public void update(@NotNull Session object) {
        @NotNull final String sql = String.format("update %s set user_id=?, created=?, role=?" +
                "where id=?", getTableName());
        try {
            @NotNull PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, object.getUserId());
            statement.setTimestamp(2, new Timestamp(object.getDate().getTime()));
            statement.setString(3, object.getRole().toString());
            statement.setString(4, object.getId());
            statement.executeUpdate();
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public @Nullable Session add(@Nullable String userId, @NotNull Session model) {
        if (userId == null) return null;
        model.setUserId(userId);
        return add(model);
    }
}
