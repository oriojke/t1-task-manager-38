package ru.t1.didyk.taskmanager.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.api.repository.IUserOwnedRepository;
import ru.t1.didyk.taskmanager.exception.field.UserIdEmptyException;
import ru.t1.didyk.taskmanager.model.AbstractUserOwnedModel;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    public AbstractUserOwnedRepository(@NotNull Connection connection) {
        super(connection);
    }

    @Override
    public void clear(@Nullable final String userId) {
        @NotNull final List<M> models = findAll(userId);
        removeAll(models);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@Nullable final String userId) {
        if (userId == null) return Collections.emptyList();
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull final String sql = String.format("select * from %s where user_id='%s'", getTableName(), userId);
        try {
            @NotNull final Statement statement = connection.createStatement();
            @NotNull final ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next())
                result.add(fetch(resultSet));
        } catch (Exception e) {
            throw e;
        }
        return result;
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final String userId, @Nullable final Comparator<M> comparator) {
        @NotNull final List<M> result = findAll(userId);
        result.sort(comparator);
        return result;
    }

    @Nullable
    @Override
    public abstract M add(@Nullable final String userId, @NotNull final M model);

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        return findOneById(userId, id) != null;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || id == null) return null;
        @Nullable M result = null;
        @NotNull final String sql = String.format("select * from %s where user_id='%s' limit 1", getTableName(), userId);
        try {
            @NotNull final Statement statement = connection.createStatement();
            @NotNull final ResultSet resultSet = statement.executeQuery(sql);
            if (resultSet.next())
                result = fetch(resultSet);
        } catch (Exception e) {
            throw e;
        }
        return result;
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        return findAll(userId).get(index);
    }

    @Override
    @SneakyThrows
    public int getSize(@Nullable final String userId) {
        /*return (int) models
                .stream()
                .filter(m -> userId.equals(m.getUserId()))
                .count();*/
        int result = 0;
        @NotNull final String sql = String.format("select count(*) from %s where user_id='%s'", getTableName(), userId);
        try {
            @NotNull final Statement statement = connection.createStatement();
            @NotNull final ResultSet resultSet = statement.executeQuery(sql);
            if (resultSet.next())
                result = resultSet.getInt("count");
        } catch (Exception e) {
            throw e;
        }
        return result;
    }

    @Nullable
    @Override
    public M remove(@Nullable final String userId, @Nullable final M model) {
        if (userId == null || model == null) return null;
        return removeById(userId, model.getId());
    }

    @Nullable
    @Override
    public M removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || id == null) return null;
        @Nullable final M model = findOneById(userId, id);
        if (model == null) return null;
        return remove(model);
    }

    @Nullable
    @Override
    public M removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        @Nullable final M model = findOneByIndex(userId, index);
        if (model == null) return null;
        return remove(model);
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        /*models
                .stream()
                .filter(m -> userId.equals(m.getUserId()))
                .collect(Collectors.toList())
                .forEach(m -> models.remove(m));*/
        findAll(userId).forEach(model -> remove(model));
    }
}
