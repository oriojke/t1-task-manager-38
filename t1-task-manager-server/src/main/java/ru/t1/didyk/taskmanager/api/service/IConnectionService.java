package ru.t1.didyk.taskmanager.api.service;

import org.apache.commons.dbcp2.BasicDataSource;
import org.jetbrains.annotations.NotNull;

import java.sql.Connection;

public interface IConnectionService {

    @NotNull Connection getConnection();

    @NotNull BasicDataSource dataSource();

}
