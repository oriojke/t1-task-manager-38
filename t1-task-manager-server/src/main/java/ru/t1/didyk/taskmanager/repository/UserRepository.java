package ru.t1.didyk.taskmanager.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.api.repository.IUserRepository;
import ru.t1.didyk.taskmanager.enumerated.Role;
import ru.t1.didyk.taskmanager.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public UserRepository(@NotNull Connection connection) {
        super(connection);
    }

    @Nullable
    @Override
    public User findById(@NotNull final String id) {
        return findOneById(id);
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByLogin(@NotNull final String login) {
        User result = null;
        @NotNull final String sql = String.format("select * from %s where login='%s' limit 1", getTableName(), login);
        try {
            @NotNull final Statement statement = connection.createStatement();
            @NotNull final ResultSet resultSet = statement.executeQuery(sql);
            if (resultSet.next())
                result = fetch(resultSet);
        } catch (Exception e) {
            throw e;
        }
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByEmail(@NotNull final String email) {
        User result = null;
        @NotNull final String sql = String.format("select * from %s where email='%s' limit 1", getTableName(), email);
        try {
            @NotNull final Statement statement = connection.createStatement();
            @NotNull final ResultSet resultSet = statement.executeQuery(sql);
            if (resultSet.next())
                result = fetch(resultSet);
        } catch (Exception e) {
            throw e;
        }
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Boolean isLoginExists(@NotNull final String login) {
        int result = 0;
        @NotNull final String sql = String.format("select count(*) from %s where login='%s'", getTableName(), login);
        try {
            @NotNull final Statement statement = connection.createStatement();
            @NotNull final ResultSet resultSet = statement.executeQuery(sql);
            if (resultSet.next())
                result = resultSet.getInt("count");
        } catch (Exception e) {
            throw e;
        }
        return result > 0;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Boolean isEmailExists(@NotNull final String email) {
        /*return models
                .stream()
                .anyMatch(user -> email.equals(user.getEmail()));*/
        int result = 0;
        @NotNull final String sql = String.format("select count(*) from %s where email='%s'", getTableName(), email);
        try {
            @NotNull final Statement statement = connection.createStatement();
            @NotNull final ResultSet resultSet = statement.executeQuery(sql);
            if (resultSet.next())
                result = resultSet.getInt("count");
        } catch (Exception e) {
            throw e;
        }
        return result > 0;
    }

    @Override
    protected String getTableName() {
        return "users";
    }

    @Override
    @SneakyThrows
    protected @NotNull User fetch(@NotNull ResultSet row) {
        @NotNull User user = new User();
        user.setEmail(row.getString("email"));
        user.setLocked(row.getBoolean("is_locked"));
        user.setFirstName(row.getString("first_name"));
        user.setMiddleName(row.getString("middle_name"));
        user.setLastName(row.getString("last_name"));
        user.setRole(Role.valueOf(row.getString("role")));
        user.setPasswordHash(row.getString("password"));
        user.setId(row.getString("id"));
        user.setLogin(row.getString("login"));
        return user;
    }

    @Override
    @SneakyThrows
    public @Nullable User add(@Nullable User model) {
        @NotNull final String sql = String.format("insert into %s (id, first_name, middle_name, last_name, login, password, email, is_locked, role)" +
                "values (?, ?, ?, ?, ?, ?, ?, ?, ?)", getTableName());
        try {
            @NotNull PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, model.getId());
            statement.setString(2, model.getFirstName());
            statement.setString(3, model.getMiddleName());
            statement.setString(4, model.getLastName());
            statement.setString(5, model.getLogin());
            statement.setString(6, model.getPasswordHash());
            statement.setString(7, model.getEmail());
            statement.setBoolean(8, model.getLocked());
            statement.setString(9, model.getRole().toString());
            statement.executeUpdate();
        } catch (Exception e) {
            throw e;
        }
        return model;
    }

    @Override
    @SneakyThrows
    public void update(@NotNull User object) {
        @NotNull final String sql = String.format("update %s set first_name=?, middle_name=?, last_name=?, login=?, password=?, email=?, is_locked=?, role=?" +
                "where id=?", getTableName());
        try {
            @NotNull PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, object.getFirstName());
            statement.setString(2, object.getMiddleName());
            statement.setString(3, object.getLastName());
            statement.setString(4, object.getLogin());
            statement.setString(5, object.getPasswordHash());
            statement.setString(6, object.getEmail());
            statement.setBoolean(7, object.getLocked());
            statement.setString(8, object.getRole().toString());
            statement.setString(9, object.getId());
            statement.executeUpdate();
        } catch (Exception e) {
            throw e;
        }
    }
}