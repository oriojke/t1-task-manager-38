package ru.t1.didyk.taskmanager.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.api.repository.IProjectRepository;
import ru.t1.didyk.taskmanager.enumerated.Status;
import ru.t1.didyk.taskmanager.model.Project;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;

public class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    public ProjectRepository(@NotNull Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    public Project create(@NotNull final String userId, @NotNull final String name, @NotNull final String description) {
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        return add(project);
    }

    @NotNull
    @Override
    public Project create(@NotNull final String userId, @NotNull final String name) {
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setUserId(userId);
        return add(project);
    }

    @Override
    protected String getTableName() {
        return "projects";
    }

    @Override
    @SneakyThrows
    protected @NotNull Project fetch(@NotNull ResultSet row) {
        @NotNull Project project = new Project();
        project.setId(row.getString("id"));
        project.setName(row.getString("name"));
        project.setDescription(row.getString("description"));
        project.setUserId(row.getString("user_id"));
        project.setStatus(Status.toStatus(row.getString("status")));
        project.setCreated(row.getTimestamp("created"));
        return project;
    }

    @Override
    @SneakyThrows
    public @Nullable Project add(@Nullable Project model) {
        @NotNull final String sql = String.format("insert into %s (id, name, description, user_id, created, status)" +
                "values (?, ?, ?, ?, ?, ?)", getTableName());
        try {
            @NotNull PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, model.getId());
            statement.setString(2, model.getName());
            statement.setString(3, model.getDescription());
            statement.setString(4, model.getUserId());
            statement.setTimestamp(5, new Timestamp(model.getCreated().getTime()));
            statement.setString(6, Status.NOT_STARTED.toString());
            statement.executeUpdate();
        } catch (Exception e) {
            throw e;
        }
        return model;
    }

    @Override
    @SneakyThrows
    public void update(@NotNull Project object) {
        @NotNull final String sql = String.format("update %s set name=?, description=?, user_id=?, created=?, status=? " +
                "where id=?", getTableName());
        try {
            @NotNull PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, object.getName());
            statement.setString(2, object.getDescription());
            statement.setString(3, object.getUserId());
            statement.setTimestamp(4, new Timestamp(object.getCreated().getTime()));
            statement.setString(5, object.getStatus().toString());
            statement.setString(6, object.getId());
            statement.executeUpdate();
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    @Nullable
    public Project add(@Nullable String userId, @NotNull Project model) {
        if (userId == null) return null;
        model.setUserId(userId);
        return add(model);
    }
}
