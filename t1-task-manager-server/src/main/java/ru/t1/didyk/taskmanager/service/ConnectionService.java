package ru.t1.didyk.taskmanager.service;

import lombok.SneakyThrows;
import org.apache.commons.dbcp2.BasicDataSource;
import org.jetbrains.annotations.NotNull;
import ru.t1.didyk.taskmanager.api.service.IConnectionService;
import ru.t1.didyk.taskmanager.api.service.IDatabaseProperty;

import java.sql.Connection;

public class ConnectionService implements IConnectionService {

    @NotNull
    private final IDatabaseProperty databaseProperty;

    public ConnectionService(@NotNull IDatabaseProperty databaseProperty) {
        this.databaseProperty = databaseProperty;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Connection getConnection() {
        @NotNull Connection connection = dataSource().getConnection();
        connection.setAutoCommit(false);
        return connection;
    }

    @NotNull
    @Override
    public BasicDataSource dataSource() {
        final BasicDataSource result = new BasicDataSource();
        result.setUrl(databaseProperty.getDatabaseUrl());
        result.setUsername(databaseProperty.getDatabaseUser());
        result.setPassword(databaseProperty.getDatabasePassword());
        result.setMinIdle(5);
        result.setMaxIdle(10);
        result.setAutoCommitOnReturn(false);
        result.setMaxOpenPreparedStatements(100);
        return result;
    }
}
