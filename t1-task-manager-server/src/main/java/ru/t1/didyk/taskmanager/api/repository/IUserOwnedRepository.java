package ru.t1.didyk.taskmanager.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.model.AbstractUserOwnedModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> {

    void clear(@Nullable String userId);

    @Nullable
    List<M> findAll(@Nullable String userId);

    @Nullable
    List<M> findAll(@Nullable String userId, @Nullable Comparator<M> comparator);

    @Nullable
    List<M> findAll();

    @Nullable
    M add(@Nullable String userId, @NotNull M model);

    @Nullable
    M add(@NotNull M model);

    boolean existsById(@Nullable String userId, @Nullable String id);

    boolean existsById(@Nullable String id);

    @Nullable
    M findOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    M findOneByIndex(@Nullable String userId, @Nullable Integer index);

    int getSize(@Nullable String userId);

    @Nullable
    M remove(@Nullable String userId, @Nullable M model);

    @Nullable
    M remove(@Nullable M model);

    @Nullable
    M removeById(@Nullable String userId, @Nullable String id);

    @Nullable
    M removeByIndex(@Nullable String userId, @Nullable Integer index);

    void removeAll(@Nullable String userId);

    @NotNull
    Collection<M> add(@NotNull Collection<M> models);

    @NotNull
    Collection<M> set(@NotNull Collection<M> models);

    void update(@NotNull final M object);

}
