package ru.t1.didyk.taskmanager.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.didyk.taskmanager.dto.request.*;
import ru.t1.didyk.taskmanager.dto.response.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IDomainEndpoint extends IEndpoint {

    @NotNull
    String NAME = "DomainEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static IDomainEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IDomainEndpoint newInstance(@NotNull final IConnectionProvider connectionProvider) {
        return IEndpoint.newInstance(connectionProvider, NAME, SPACE, PART, IDomainEndpoint.class);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IDomainEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        return IEndpoint.newInstance(host, port, NAME, SPACE, PART, IDomainEndpoint.class);
    }

    @NotNull
    @WebMethod
    DataBackupLoadResponse domainBackupLoad(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataBackupLoadRequest request
    );

    @NotNull
    @WebMethod
    DataBackupSaveResponse domainBackupSave(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataBackupSaveRequest request
    );

    @NotNull
    @WebMethod
    DataBase64LoadResponse domainBase64Load(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataBase64LoadRequest request
    );

    @NotNull
    @WebMethod
    DataBase64SaveResponse domainBase64Save(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataBase64SaveRequest request
    );

    @NotNull
    @WebMethod
    DataBinaryLoadResponse domainBinaryLoad(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataBinaryLoadRequest request
    );

    @NotNull
    @WebMethod
    DataBinarySaveResponse domainBinarySave(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataBinarySaveRequest request
    );

    @NotNull
    @WebMethod
    DataJsonLoadFasterXmlResponse domainJsonLoadFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataJsonLoadFasterXmlRequest request
    );

    @NotNull
    @WebMethod
    DataJsonLoadJaxBResponse domainJsonLoadJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataJsonLoadJaxBRequest request
    );

    @NotNull
    @WebMethod
    DataJsonSaveFasterXmlResponse domainJsonSaveFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataJsonSaveFasterXmlRequest request
    );

    @NotNull
    @WebMethod
    DataJsonSaveJaxBResponse domainJsonSaveJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataJsonSaveJaxBRequest request
    );

    @NotNull
    @WebMethod
    DataXmlLoadFasterXmlResponse domainXmlLoadFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataXmlLoadFasterXmlRequest request
    );

    @NotNull
    @WebMethod
    DataXmlLoadJaxBResponse domainXmlLoadJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataXmlLoadJaxBRequest request
    );

    @NotNull
    @WebMethod
    DataXmlSaveFasterXmlResponse domainXmlSaveFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataXmlSaveFasterXmlRequest request
    );

    @NotNull
    @WebMethod
    DataXmlSaveJaxBResponse domainXmlSaveJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataXmlSaveJaxBRequest request
    );

    @NotNull
    @WebMethod
    DataYamlLoadFasterXmlResponse domainYamlLoadFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataYamlLoadFasterXmlRequest request
    );

    @NotNull
    @WebMethod
    DataYamlSaveFasterXmlResponse domainYamlSaveFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataYamlSaveFasterXmlRequest request
    );

}
