package ru.t1.didyk.taskmanager.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public class DataYamlSaveFasterXmlRequest extends AbstractUserRequest {
    public DataYamlSaveFasterXmlRequest(@Nullable String token) {
        super(token);
    }
}
