package ru.t1.didyk.taskmanager.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public class DataJsonSaveJaxBRequest extends AbstractUserRequest {
    public DataJsonSaveJaxBRequest(@Nullable String token) {
        super(token);
    }
}
